#include "Transform.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_access.hpp>

Transform::Transform()
{
	m_scale = glm::vec3(1);
}

Transform::Transform(const glm::mat4 &m)
{
	setMat4(m);
}

void Transform::setTranslation(const glm::vec3 &translation)
{
	m_translation = translation;
}

void Transform::setOrientation(const glm::quat &orientation)
{
	m_orientation = orientation;
}

void Transform::setScale(const glm::vec3 &scale)
{
	m_scale = scale;
}

void Transform::setMat4(const glm::mat4 &matrix)
{
	glm::vec3 c[] = {
		glm::vec3(glm::column(matrix, 0)),
		glm::vec3(glm::column(matrix, 1)),
		glm::vec3(glm::column(matrix, 2)),
		glm::vec3(glm::column(matrix, 3)) };

	glm::vec3 scale = glm::vec3(glm::length(c[0]), glm::length(c[1]), glm::length(c[2]));
	glm::quat orientation = glm::quat_cast(matrix);
	glm::vec3 translation = c[3];

	setScale(scale);
	setOrientation(orientation);
	setTranslation(translation);
}

const glm::vec3 & Transform::getTranslation() const
{
	return m_translation;
}

const glm::quat & Transform::getOrientation() const
{
	return m_orientation;
}

const glm::vec3 & Transform::getScale() const
{
	return m_scale;
}

glm::mat4 Transform::getMat4() const
{
	glm::mat4 T = glm::translate(glm::mat4(), m_translation);
	glm::mat4 R = glm::mat4_cast(m_orientation);
	glm::mat4 S = glm::scale(glm::mat4(), m_scale);

	return T * R * S;
}

glm::mat4 Transform::getInvMat4() const
{
	glm::mat4 T = glm::translate(glm::mat4(), -m_translation);
	glm::mat4 R = glm::mat4_cast(glm::conjugate(m_orientation));
	glm::mat4 S = glm::scale(glm::mat4(), 1.0f/m_scale);

	return S * R * T;
}

Transform Transform::interpolate(const Transform & t0, const Transform & t1, float k)
{
	Transform t;
	k = glm::clamp(k, 0.0f, 1.0f);
	float k0 = 1.0f-k;
	float k1 = k;

	t.setTranslation(glm::mix(t0.getTranslation(), t1.getTranslation(), k));
	t.setOrientation(glm::slerp(t0.getOrientation(), t1.getOrientation(), k));
	t.setScale(glm::mix(t0.getScale(), t1.getScale(), k));

	return t;
}
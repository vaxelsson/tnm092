#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

class Transform
{
public:
	Transform();
	Transform(const glm::mat4 &m);

	void setTranslation(const glm::vec3 &translation);
	void setOrientation(const glm::quat &orientation);
	void setScale(const glm::vec3 &scale);

	void setMat4(const glm::mat4 &matrix);

	const glm::vec3 & getTranslation() const;
	const glm::quat & getOrientation() const;
	const glm::vec3 & getScale() const;

	glm::mat4 getMat4() const;
	glm::mat4 getInvMat4() const;

	static Transform interpolate(const Transform & t0, const Transform & t1, float k);

private:
	glm::vec3 m_translation;
	glm::quat m_orientation;
	glm::vec3 m_scale;
};
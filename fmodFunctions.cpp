#include "fmodFunctions.h"
#include <stdlib.h>

#define OUTPUTRATE          48000
#define SPECTRUMSIZE        1024
#define SPECTRUMRANGE       ((float)OUTPUTRATE / 2.0f)      /* 0 to nyquist */
#define BINSIZE (SPECTRUMRANGE / (float)SPECTRUMSIZE)

void ERRCHECK(FMOD_RESULT result){
	if (result != FMOD_OK){
		printf("fmod error! (%d) %s\n", result, FMOD_ErrorString(result));
		exit(-1);
	}
}

float* fmodFunctions::spectrum = new float[SPECTRUMSIZE];

fmodFunctions::fmodFunctions() : 
system(NULL),
sound(NULL),
channel(0)
{
	dominanthz = 0;
	dominantnote = 0;
	binsize = BINSIZE;
	initFmod();
};

fmodFunctions::~fmodFunctions(){
	result = sound->release();
	ERRCHECK(result);
	result = system->release();
	ERRCHECK(result);
};

void fmodFunctions::initFmod(){
	//Create a System object and initialize.
	result = FMOD::System_Create(&system);
	ERRCHECK(result);

	result = system->getVersion(&version);
	ERRCHECK(result);

	if (version < FMOD_VERSION){
		printf("Error!  You are using an old version of FMOD %08x.  This program requires %08x\n", version, FMOD_VERSION);
	}

	printf("TJA\n");

	//System initialization
	#ifdef __WIN32
	result = system->setOutput(FMOD_OUTPUTTYPE_DSOUND);
	ERRCHECK(result);
	#endif

	//Enumerate playback devices
	driver = 0;
	result = system->setDriver(driver);
	ERRCHECK(result);

	result = system->setSoftwareFormat(OUTPUTRATE, FMOD_SOUND_FORMAT_PCM16, 1, 0, FMOD_DSP_RESAMPLER_LINEAR);
	ERRCHECK(result);

	result = system->init(32, FMOD_INIT_NORMAL, 0);
	ERRCHECK(result);

	//Create a sound to record to.
	memset(&exinfo, 0, sizeof(FMOD_CREATESOUNDEXINFO));
	exinfo.cbsize = sizeof(FMOD_CREATESOUNDEXINFO);
	exinfo.numchannels = 1;
	exinfo.format = FMOD_SOUND_FORMAT_PCM16;
	exinfo.defaultfrequency = OUTPUTRATE;
	exinfo.length = exinfo.defaultfrequency * sizeof(short)* exinfo.numchannels * 5;

	result = system->createSound(0, FMOD_2D | FMOD_SOFTWARE | FMOD_LOOP_NORMAL | FMOD_OPENUSER, &exinfo, &sound);
	ERRCHECK(result);

	system->setDSPBufferSize(256, 1);
	system->setStreamBufferSize(65536, FMOD_TIMEUNIT_RAWBYTES);

	ERRCHECK(result);

	//Start the interface
	recorddriver = 0;
	result = system->recordStart(recorddriver, sound, true);
	ERRCHECK(result);

	Sleep(100);

	result = system->playSound(FMOD_CHANNEL_REUSE, sound, false, &channel);
	ERRCHECK(result);

	/* Dont hear what is being recorded otherwise it will feedback.  Spectrum analysis is done before volume scaling in the DSP chain */
	result = channel->setVolume(0);
	ERRCHECK(result);
}

float fmodFunctions::runFmod(){
	result = channel->getSpectrum(spectrum, SPECTRUMSIZE, 0, FMOD_DSP_FFT_WINDOW_TRIANGLE);
	ERRCHECK(result);

	max = 0;

	for (count = 0; count < SPECTRUMSIZE; count++)
	{
		if (spectrum[count] > 0.01f && spectrum[count] > max)
		{
			max = spectrum[count];
			bin = count;
		}
	}

	dominanthz = (float)bin * BINSIZE;       /* dominant frequency min */

	std::cout << max << std::endl;
	std::cout << bin << std::endl;
	std::cout << BINSIZE << std::endl;
	
	system->update();
	return max;
}
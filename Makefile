FLAGS = -stdlib=libstdc++ -I fmodex_mac/inc
LDFLAGS = -v -lsgct -lfmodex -framework Opengl -framework Cocoa -framework IOKit -stdlib=libstdc++ -Lfmodex_mac/lib
LDFLAGS += -lglew

all: main.o fmodFunctions.o Camera.o Entity.o Transform.o objloader.o
	clang++ -o main main.o fmodFunctions.o Camera.o Entity.o Transform.o objloader.o $(LDFLAGS)
	./main -config "single.xml"

main.o: main.cpp
	clang++ -c -O3 main.cpp $(FLAGS)

fmodFunctions.o: fmodFunctions.cpp
	clang++ -c -O3 fmodFunctions.cpp $(FLAGS)

Camera.o: Camera.cpp
	clang++ -c -O3 Camera.cpp $(FLAGS)

Entity.o: Entity.cpp
	clang++ -c -O3 Entity.cpp $(FLAGS)

Transform.o: Transform.cpp
	clang++ -c -O3 Transform.cpp $(FLAGS)

objloader.o: objloader.cpp
	clang++ -c -O3 objloader.cpp $(FLAGS)

clean:
	$(RM) *.o main
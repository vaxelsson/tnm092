#ifndef FMODFUNCTIONS_H
#define FMODFUNCTIONS_H
#include <stdlib.h>
#include <iostream>

#include "api/inc/fmod.hpp"
#include "api/inc/fmod_errors.h"
#ifdef _WIN32
#include <windows.h>
#endif
#include <stdio.h>
//#include <conio.h>
#include <math.h>

class fmodFunctions{
	public:
		fmodFunctions();
		~fmodFunctions();
		void initFmod();
		void runFmod();
		
		FMOD::System          *system;
		FMOD::Sound           *sound;
		FMOD::Channel         *channel;
		FMOD_RESULT            result;
		FMOD_CREATESOUNDEXINFO exinfo;
		int                    key, driver, recorddriver, numdrivers, count, bin;
		unsigned int           version;

		static float* spectrum;
		float        dominanthz;
		float        max;
		int          dominantnote;
		float        binsize;
};

#endif
#include <stdlib.h>
#include <stdio.h>
#include <fstream>
//#include <libavcodec/avcodec.h>
//#include <libavformat/avformat.h>
#include "sgct.h"
#include "fmodFunctions.h"
#include "Camera.h"
#include "Transform.h"
#include "objloader.hpp"

#ifdef __APPLE__
#include <unistd.h>
#endif

sgct::Engine * gEngine;

void myDrawFun();
void myPreSyncFun();
void myInitOGLFun();
void myEncodeFun();
void myDecodeFun();
void myCleanUpFun();

void		loadModel( std::string filename, GLuint &vao, GLuint *vbo, GLint &verts);
bool 		readKeyframes(Transform* frames, unsigned int frameCount, const char * file);
Transform 	getInterpolatedFrame(float t);
void 		readAndSetTextureData(int frame);

bool 		initFFMPEG();
void 		destroyFFMPEG();

size_t boxTexture;
sgct_utils::SGCTBox * myBox = NULL;
GLint Matrix_Loc = -1;

fmodFunctions* 		gFmodFunc;
Transform* 			gKeyframes;
Camera*				gCamera;
double 				gAnimLength;
GLuint 				gBGTexture;

//AVFormatContext 	*pFormatCtx;

//variables to share across cluster
sgct::SharedDouble 	curr_time(0.0);
sgct::SharedDouble 	anim_time(0.0);
sgct::SharedInt 	curr_frame(0);

#define FRAME_COUNT 201
#define FRAME_FPS 25.0
#define VIDEO_SRC "dome.mp4"

enum VBO_INDEXES { VBO_POSITIONS = 0, VBO_UV, VBO_NORMALS };

GLuint vbo[3];
GLuint vao = 0;
GLsizei verts = 0;

GLuint vboBG[3];
GLuint vaoBG = 0;
GLsizei vertsBG = 0;

int main( int argc, char** argv )
{
	gKeyframes = new Transform[FRAME_COUNT];
	readKeyframes(gKeyframes, FRAME_COUNT, "camDome.txt");
	gAnimLength = FRAME_COUNT / (double)FRAME_FPS;

	gEngine = new sgct::Engine( argc, argv );

	gEngine->setInitOGLFunction( myInitOGLFun );
	gEngine->setDrawFunction( myDrawFun );
	gEngine->setPreSyncFunction( myPreSyncFun );
	gEngine->setCleanUpFunction( myCleanUpFun );

	if( !gEngine->init( sgct::Engine::OpenGL_3_3_Core_Profile ) )
	{
		delete gEngine;
		return EXIT_FAILURE;
	}

	sgct::SharedData::instance()->setEncodeFunction(myEncodeFun);
	sgct::SharedData::instance()->setDecodeFunction(myDecodeFun);

	// Main loop
	gEngine->render();

	// Clean up
	delete gEngine;

	// Exit program
	exit( EXIT_SUCCESS );
}

void myDrawFun()
{
	glEnable( GL_DEPTH_TEST );
	//glEnable( GL_CULL_FACE );

	//create scene transform (animation)
	glm::mat4 scene_mat = glm::translate( glm::mat4(1.0f), glm::vec3( 0.0f, 0.0f, 0.0f) );
	//scene_mat = glm::rotate( scene_mat, static_cast<float>( curr_time.getVal() * speed ), glm::vec3(0.0f, -1.0f, 0.0f));
	//scene_mat = glm::rotate( scene_mat, static_cast<float>( curr_time.getVal() * (speed/2.0) ), glm::vec3(1.0f, 0.0f, 0.0f));

	Transform T = getInterpolatedFrame(anim_time.getVal());
	gCamera->setTransform(T);
	glm::mat4 MVP = gEngine->getActiveModelViewProjectionMatrix() * gCamera->getViewMatrix() * scene_mat;

	glActiveTexture(GL_TEXTURE0);
	//glBindTexture( GL_TEXTURE_2D, sgct::TextureManager::instance()->getTextureByName("box") );
	glBindTexture( GL_TEXTURE_2D, sgct::TextureManager::instance()->getTextureByHandle(boxTexture) );

	sgct::ShaderManager::instance()->bindShaderProgram( "xform" );

	glUniformMatrix4fv(Matrix_Loc, 1, GL_FALSE, &MVP[0][0]);

	//draw the box
	//myBox->draw();

	// ------ draw model --------------- //
	glBindVertexArray(vao);
	glDrawArrays(GL_TRIANGLES, 0, verts );
	glBindVertexArray(GL_FALSE); //unbind
	// ----------------------------------//

	sgct::ShaderManager::instance()->unBindShaderProgram();

	glDisable( GL_CULL_FACE );
	glDisable( GL_DEPTH_TEST );
}

void myPreSyncFun()
{
	if( gEngine->isMaster() )
	{
		static double old_t = 0;
		double t = sgct::Engine::getTime();
		double dt = t - old_t;
		old_t = t;

		double at = anim_time.getVal() + dt;
		if(at > gAnimLength)
			at = at - gAnimLength;

		int f = (int)( (at / gAnimLength) * (FRAME_COUNT));

		curr_time.setVal( t );
		anim_time.setVal( at );
		curr_frame.setVal( f );
	}

	readAndSetTextureData(curr_frame.getVal());
}

void myInitOGLFun()
{
	sgct::TextureManager::instance()->setAnisotropicFilterSize(8.0f);
	sgct::TextureManager::instance()->setCompression(sgct::TextureManager::S3TC_DXT);
	sgct::TextureManager::instance()->loadTexure(boxTexture, "box", "box.png", true);

	myBox = new sgct_utils::SGCTBox(2.0f, sgct_utils::SGCTBox::Regular);
	//myBox = new sgct_utils::SGCTBox(2.0f, sgct_utils::SGCTBox::CubeMap);
	//myBox = new sgct_utils::SGCTBox(2.0f, sgct_utils::SGCTBox::SkyBox);

	loadModel( "geometry.obj", vao, vbo, verts);
	loadModel( "halfspheremiddle.obj", vaoBG, vboBG, vertsBG);

	//Set up backface culling
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW); //our polygon winding is counter clockwise

	sgct::ShaderManager::instance()->addShaderProgram( "xform",
			"SimpleVertexShader.vertexshader",
			"SimpleFragmentShader.fragmentshader" );

	sgct::ShaderManager::instance()->bindShaderProgram( "xform" );

	Matrix_Loc = sgct::ShaderManager::instance()->getShaderProgram( "xform").getUniformLocation( "MVP" );
	GLint Tex_Loc = sgct::ShaderManager::instance()->getShaderProgram( "xform").getUniformLocation( "Tex" );
	glUniform1i( Tex_Loc, 0 );

	sgct::ShaderManager::instance()->unBindShaderProgram();

	gCamera = new Camera();

	fmodFunctions* gFmodFunc = new fmodFunctions();
}

void myEncodeFun()
{
	sgct::SharedData::instance()->writeDouble(&curr_time);
	sgct::SharedData::instance()->writeDouble(&anim_time);
	sgct::SharedData::instance()->writeInt(&curr_frame);
}

void myDecodeFun()
{
	sgct::SharedData::instance()->readDouble(&curr_time);
	sgct::SharedData::instance()->readDouble(&anim_time);
	sgct::SharedData::instance()->readInt(&curr_frame);
}

void myCleanUpFun()
{
	if(myBox != NULL)
		delete myBox;

	if(gKeyframes)
		delete[] gKeyframes;

	if(gCamera)
		delete gCamera;

	if(gFmodFunc)
		delete gFmodFunc;
}

void loadModel( std::string filename, GLuint &vao, GLuint *vbo, GLint &verts)
{
	// Read our .obj file
	std::vector<glm::vec3> positions;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals;
	
	//if successful
	if( loadOBJ( filename.c_str(), positions, uvs, normals) )
	{
		//store the number of triangles
		verts = static_cast<GLsizei>( positions.size() );
		
		//create VAO
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);

		//init VBOs
		for(unsigned int i=0; i<3; i++)
			vbo[i] = GL_FALSE;
		glGenBuffers(3, &vbo[0]);
		
		
		glBindBuffer(GL_ARRAY_BUFFER, vbo[ VBO_POSITIONS ] );
		glBufferData(GL_ARRAY_BUFFER, positions.size() * sizeof(glm::vec3), &positions[0], GL_STATIC_DRAW);
		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(
			2,                  // attribute
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			reinterpret_cast<void*>(0) // array buffer offset
		);

		if( uvs.size() > 0 )
		{
			glBindBuffer(GL_ARRAY_BUFFER, vbo[ VBO_UV ] );
			glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);
			// 2nd attribute buffer : UVs
			glEnableVertexAttribArray(1);
			glVertexAttribPointer(
				1,                                // attribute
				2,                                // size
				GL_FLOAT,                         // type
				GL_FALSE,                         // normalized?
				0,                                // stride
				reinterpret_cast<void*>(0) // array buffer offset
			);
		}
		else
			sgct::MessageHandler::instance()->print("Warning: Model is missing UV data.\n");

		if( normals.size() > 0 )
		{
			glBindBuffer(GL_ARRAY_BUFFER, vbo[ VBO_NORMALS ] );
			glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);
			// 3nd attribute buffer : Normals
			glEnableVertexAttribArray(0);
			glVertexAttribPointer(
				0,                                // attribute
				3,                                // size
				GL_FLOAT,                         // type
				GL_FALSE,                         // normalized?
				0,                                // stride
				reinterpret_cast<void*>(0) // array buffer offset
			);
		}
		else
			sgct::MessageHandler::instance()->print("Warning: Model is missing normal data.\n");

		glBindVertexArray(GL_FALSE); //unbind VAO

		//clear vertex data that is uploaded on GPU
		positions.clear();
		uvs.clear();
		normals.clear();

		//print some usefull info
		sgct::MessageHandler::instance()->print("Model '%s' loaded successfully (%u vertices, VAO: %u, VBOs: %u %u %u).\n",
			filename.c_str(),
			verts,
			vao,
			vbo[VBO_POSITIONS],
			vbo[VBO_UV],
			vbo[VBO_NORMALS] );
	}
	else
		sgct::MessageHandler::instance()->print("Failed to load model '%s'!\n", filename.c_str() );

}

bool readKeyframes(Transform* frames, unsigned int frameCount, const char * filename)
{
	std::ifstream ifs;
	ifs.open(filename);

	if(!ifs.is_open()){
		printf("Could not read file %s \n", filename);
		return false;
	}

	glm::mat4 m;
	float f[16];

	for(int frame=0; ifs.good(), frame<frameCount; ++frame)
	{
		for(int i=0; i<16; ++i)
			ifs >> f[i];

		m = glm::mat4(
			f[0], 	f[1], 	f[2], 	f[3],
			f[4], 	f[5], 	f[6], 	f[7],
			f[8], 	f[9], 	f[10], 	f[11],
			f[12], 	f[13], 	f[14], 	f[15]);

		frames[frame] = Transform(glm::transpose(m));
	}

	printf("Done reading keyframe data \n");
	return true;
}

Transform getInterpolatedFrame(float t)
{
	float percent = t/gAnimLength;
	int lowFrame = (int)(FRAME_COUNT * percent);
	int highFrame = lowFrame+1;
	if(highFrame >= FRAME_COUNT)
		highFrame = 0;

	float k = t - (lowFrame / (float)FRAME_COUNT);

	return Transform::interpolate(gKeyframes[lowFrame], gKeyframes[highFrame], k);
}

void readAndSetTextureData(int frame)
{
	
}
/*
bool initFFMPEG()
{
	av_register_all();

	if(avformat_open_input(&pFormatCtx, VIDEO_SRC, NULL, 0) != 0)
		return false;

	// Retrieve stream information
	if(avformat_find_stream_info(pFormatCtx, NULL) < 0)
		return false;

	AVCodecContext *pCodecCtx;
	// Find the first video stream
	int videoStream=-1;
	for(int i=0; i<pFormatCtx->nb_streams; i++)
		if(pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) {
			videoStream=i;
			break;
		}
	if(videoStream==-1)
	  return false; // Didn't find a video stream

	// Get a pointer to the codec context for the video stream
	pCodecCtx=pFormatCtx->streams[videoStream]->codec;

	AVCodec *pCodec;

	// Find the decoder for the video stream
	pCodec=avcodec_find_decoder(pCodecCtx->codec_id);
	if(pCodec==NULL) {
		fprintf(stderr, "Unsupported codec!\n");
		return false; // Codec not found
	}
	// Open codec
	if(avcodec_open2(pCodecCtx, pCodec, NULL)<0)
		return false; // Could not open codec

	return true;
}

void destroyFFMPEG()
{
	avformat_close_input(&pFormatCtx);
}
*/

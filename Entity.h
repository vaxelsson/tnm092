#pragma once

#include "Transform.h"

class Entity
{
public:
	// Spatial functionality
	void setPosition(const glm::vec3 & position) 		{ m_transform.setTranslation(position); }
	void setOrientation(const glm::quat & orientation)	{ m_transform.setOrientation(orientation); }
	void setScale(const glm::vec3 & scale) 				{ m_transform.setScale(scale); }
	void setTransform(const Transform & transform) 		{ m_transform = transform; }

	void translate(const glm::vec3 & translation);
	void move(const glm::vec3 & move);

	void rotate(const glm::quat & quaternion);
	void rotate(const glm::vec3 & eulerAngles);

	const Transform & getTransform() { return m_transform; }
protected:
	Transform m_transform;
};